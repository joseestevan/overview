# OverView

Repositório com análises feitas por mim. 

Criado com o intuito de ser um espaço em que posso praticar e testar coisas novas.

### Conteúdo ###

* [Apartamentos SP](https://bitbucket.org/joseestevan/overview/src/master/Apartamentos%20SP/)

* [Premier League 2019-2020](https://bitbucket.org/joseestevan/overview/src/master/Premier%20League%202019-2020/) 

* [Data Hackers 2019 Survey](https://bitbucket.org/joseestevan/overview/src/master/Data%20Hackers%202019%20Survey/)

* [StackOverFlow 2020 Survey](https://bitbucket.org/joseestevan/overview/src/master/StackOverFlow%202020%20Survey/)

### Redes Socias ###

* [Medium](https://medium.com/@joseestevan)
* [Linkedin](https://www.linkedin.com/in/joseestevan/)
* [GitHub](https://github.com/JoseEstevan)

#### Fique à vontade para fazer download do repositório ou dos PDFs. ####



